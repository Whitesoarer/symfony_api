
# Assignment:
Headless (no UI) CMS for managing articles (CRUD).

Each article has its ID, title, body and timestamps (on creation and update).

Managing articles is the typical set of CRUD calls including reading one and all the articles. Creating/updating/deleting data is possible only if a secret token is provided (can be just one static token).   

For reading all the articles, the endpoint must allow specifying a field to sort by including whether in an ascending or descending order + basic limit/offset pagination.

The whole client-server communication must be in a JSON format. The architecture must follow Domain-Driven Design

Optional: Make the architecture ready to communicate in different formats like XML (no need for the implementation of the additional format, just make it extensible).


---
## Technical Requirements:
* ER Diagram
* PHP >= 7.1
* REST API + documentation
* Relational DB
* README
* Automated tests
---

# Realisation
## Understainding the task and facing problems
As a result We must get a REST API microservice that can be used in a more complex project. By learning Domain Driven Design I got to the point that app should not look like a standard Symfony bundle routine. But instead it should be split in a logic layers with a purpose of separating business logic from an app logic and data model(database). Trying this approach for the first time in my life was really challenging for me after using classical symfony MVC and front controller approaches years before.


## Environment:
* Php 7.3+
* Apache 2.4
* MariaDB v15.1
* Symfony 5 (skeleton instalation)
* Composer 2
* GIT (via BitBucket service)
* Installed composer packages:
* * ORM,
* * RestBundle,
* * Annotations,
* * Serializer-pack
---



#Installation process
Instalation is preety staight forward
* run composer install
* configure .env
* run `php bin/console doctrine:migrations:migrate`

---

#Workflow ER Diagram

![Diagram](https://lucid.app/publicSegments/view/6dd309a5-9335-4b61-a7b0-b47085926955/image.jpeg)

Full diagram:
[https://lucid.app/lucidchart/01194a3c-ae9b-4731-b6f3-1bb697dffc13/view](https://lucid.app/lucidchart/01194a3c-ae9b-4731-b6f3-1bb697dffc13/view)

# API documentation

Service use api calls with jason arrays (as said in assignment). "Postman" software was used as a test invironment for live api calls.
Supported call methods : GET/POST/DELETE


`[POST]/get_token` *require app secret*

Get current token. The current token is an md5 hash generated using `.env` APP_SECRET Secret variable with month-day combination

[post value] *secret* - value for validate user for token generation


---


`[ANY] /articles/list/{outputOrder?}`

Show a list of all articles. `outputOrder` can perform reverse order if set to `DESC`

[post value] *page* - page to view from (default = 0)

[post value] *per_page* - amount of positions in list per page (default = 10)

---


`[ANY] /articles/show/{id}`

Show all information about single article by id = {id}

---


`[POST] /articles/create` *require token*

Create a new Article

[post value] *title* - a title of a new Article

[post value] *body* - a body of a new Article

[post value] *token* - a generated token after `/get_token/` call

---


`[POST] /articles/update` *require token*

Update an Article

[post value] *title* - a new title of a new Article

[post value] *body* - a new body of a new Article

[post value] *token* - a generated token after `/get_token/` call

---


`[DELETE] /articles/delete` *require token*

Delete an Article

[post value] *token* - a generated token after `/get_token/` call

---

# Result
A result is a  working JSON-Api client server CRUD software using simple REST api realisation.



Realisation time took 3 days:



1. environment setup and DDD requirements research.

2. DDD realisations research. As a result of realising lack of knowledge, I made a decision make project using known way.

3. Finishing documentation, unit tests, checking alternative ways of implementing some features that were working poor. Little refactoring



#Resume


Got the main task done but failed to do a required application concept realisation.

Lack of skill and time to get grasp of DDD concept for the first time, made me fail the main assignment requirements.

### 2021/02/04
