<?php

namespace App\Entity;

/**
 * A helper trait to handle Timestamp marks in Article Entity
 *
 * @author Anton Sokolov | sokolov@twisted.solutions
 */

trait Timestamps
{
    /**
     * @ORM\Column(type="datetime")
     */
     private $createdAt;

    /**
     * @ORM\Column(type="datetime")
     */
     private $updatedAt;

    /**
     * @ORM\PrePersist()
     */
    public function createdAt()
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }

    /**
     * @ORM\PreUpdate()
     */
    public function updatedAt()
    {
        $this->updatedAt = new \DateTime();
    }
}
