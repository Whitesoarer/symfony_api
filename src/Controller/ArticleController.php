<?php
/*
 * This file is part of test assignment for a job interview.
 *
 * 2021/02/08
 */

namespace App\Controller;

use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\RequestParam;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations\FileParam;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Article;
use App\Repository\ArticleRepository;
use App\Service\TokenValidator;

/**
 * This controller handles all /article/ calls to API
 * @see README.md
 *
 * @author Anton Sokolov | sokolov@twisted.solutions
 */

class ArticleController extends AbstractController
{
    /**
     * Show a list of all articles.
     *
     * @Rest\Get ("/articles/list/{outputOrder?}", name="articles.list")
     *
     * @param string $output_order none|asc|desc
     * @param Request $request
     * @return Response
     */
    public function listArticles(Request $request, $outputOrder = 'ASC'): Response
    {
        if ($outputOrder !== 'DESC') {
             $outputOrder = 'ASC';
        }

        $page = $request -> get('page') ?? 0;
        $perPage = $request -> get('per_page') ?? 10;
        $output = [];
        $articles = $this->getDoctrine()
        ->getRepository(Article::class)
        ->showAllArticles($outputOrder, $page, $perPage);

        foreach ($articles as $article) {
            $output[] = [
                'id' => $article->getId(),
                'title' => $article->getTitle(),
            ];
        }

        return $this->json($output);
    }

    /**
     * Show one article by id.
     *
     * @Rest\Get ("/articles/show/{id}", name="articles.show")
     *
     * @param int $id
     * @param Request $request
     * @return Response
     */
    public function showArticles(int $id, Request $request): Response
    {

        $entityManager = $this->getDoctrine()->getManager();

        $article = $entityManager->getRepository(Article::class)->find($id);

        if (!$article) {
            throw $this->createNotFoundException(
                'Article not found: '.$id
            );
        }

        $output = (array) $article;

        //TODO: A better looking alternative. Neen Entity adjustment
        // $output = [
        //     'id' => $article->getId(),
        //     'title' => $article->getTitle(),
        //     'body' => $article->getBody(),
        //     'created_at' => $article->createdAt(),
        //     'updated_at' => $article->updatedAt(),
        // ];

        $entityManager->flush();
        return $this->json($output);
    }

    /**
     *
     *
     * @Rest\Post ("/articles/create", name="articles.create")
     *
     * @param Request $request
     * @return Response
     */
    public function createArticle(Request $request): Response
    {
        $articleTitle = $request->get('title');
        $articleBody = $request->get('body');
        $token = $request->get('token');


        if (!$token || $this->checkActionToken($token) != true) {
            return $this->json(['error' => 'Incorrect access token'], 403);
        }

        if ($articleTitle && $articleBody) {
            $article = new Article();
            $article -> setTitle($articleTitle);
            $article -> setBody($articleBody);
            $em = $this->getDoctrine()->getManager();

            $em -> persist($article);
            $em -> flush();
            return $this->json(['result' => 'Article created'], 201);
        }

        return $this->json(['error' => 'Article creation failed'], 500);
    }

    /**
     * @Rest\Post ("/articles/update/{id}", name="articles.update")
     *
     * @param Request $request
     * @return Response
     */

    public function updateArticle(int $id, Request $request)
    {
        $articleTitle = $request->get('title');
        $articleBody = $request->get('body');
        $token = $request->get('token');

        if (!$token || $this->checkActionToken($token) != true) {
            return $this->json(['error' => 'Incorrect access token'], 403);
        }

        $entityManager = $this->getDoctrine()->getManager();

        $article = $entityManager
            ->getRepository(Article::class)
            ->find($id);


        if (!$article) {
            throw $this->createNotFoundException(
                'Article not found: '.$id
            );
        }
        $article->setTitle($articleTitle);
        $article->setBody($articleBody);

        $entityManager->flush();
        return $this->json(['result' => 'Article updated'], 201);
    }

    /**
     * @Rest\Delete ("/articles/delete/{id}", name="articles.delete")
     */

    public function deleteArticle(int $id, Request $request)
    {
        $token = $request->get('token');

        if (!$token || $this->checkActionToken($token) != true) {
            return $this->json(['result' => 'Incorrect access token'], 403);
        }

        $entityManager = $this->getDoctrine()->getManager();

        $article = $entityManager
            ->getRepository(Article::class)
            ->find($id);

        $entityManager->remove($article);
        $entityManager->flush();

        return $this->json(['result' => 'Article removed'], 201);
    }
    /**
     * Use TokenValidator to check token
     *
     * @param string $token
     * @return bool
     * @author
     * @copyright
     */

    private function checkActionToken(string $token): bool
    {
        $tokenValidator = new TokenValidator();
        return $tokenValidator->checkToken($token);
    }
}
