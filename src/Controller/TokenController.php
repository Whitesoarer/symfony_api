<?php

namespace App\Controller;

use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Controller\MainController;

/**
 * This controller handle token generation procedure
 * Validatioon part is moved to \Service\TokenValidator
 * @see README.md
 *
 * @author Anton Sokolov | sokolov@twisted.solutions
 */

class TokenController extends AbstractController
{
    /**
     * Get current token. The current token is an md5 hash generated using
     * `.env` APP_SECRET Secret variable with month-day combination
     *
     * @Rest\Post("/get_token", name="get_token")
     *
     * @param Request $request
     * @return Response
     */

    public function getToken(Request $request)
    {
        $secret=$request->get('secret');
        if ($secret === $_ENV['APP_SECRET']) {
             $result=['token' => md5($_ENV['APP_SECRET'].date("md"))];
        } else {
            $result=['error' => 'wrong secret key:'];
        }
        return $this->json($result);
    }
}
