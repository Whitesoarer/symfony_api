<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * This is the main controller created by Symfony.
 * Used for handling http entry point to "/"
 *
 * @author Anton Sokolov | sokolov@twisted.solutions
 */

class MainController extends AbstractController
{

    /**
     * @Route("/", name="main")
     *
     * @return Response
     */

    public function index(): Response
    {
        return $this->json([
            'message' => 'Welcome to Article api!',
        ]);
    }
}
