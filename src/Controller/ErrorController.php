<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * This controller handle error state of API (Used for redirects on fail pages
 * or unpredicted behavior. As well as soft message for banned connections)
 *
 * @author Anton Sokolov | sokolov@twisted.solutions
 */

class ErrorController extends AbstractController
{
    /**
     * output error mesage with corresponding code
     * @Route ("/error/{code?}", name="error", defaults={"code": 406})
     *
     * @param int $code
     * @return Response
     */

    public function showError(int $code): Response
    {
        $errorArray = $this->getErrorArray($code);
        return $this->json(
            $errorArray,
            $errorArray['error']
        );
    }

    /**
     * Prepare array with error code for ruture json output
     *
     * @param int $code
     * @return array
     */

    private function getErrorArray(int $code = 0): array
    {
        $response=new Response();
        $response->setStatusCode($code);
        $response_status=$response->getStatusCode();


        return [
            'error' => $code,
            'error_description' => $response_status
        ];
    }
}
