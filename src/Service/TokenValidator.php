<?php

namespace App\Service;

/**
 * This is a token validation service used for some API calls
 *
 * @author Anton Sokolov
 * @contacts | sokolov@twisted.solutions
 */

class TokenValidator
{
    /**
     * check if token is generated from closed secret
     *
     * @param string $token
     * @return  bool
     */
    public function checkToken(string $token): bool
    {
        if (md5($_ENV['APP_SECRET'].date("md")) === $token) {
            return true;
        }
        return false;
    }
}
